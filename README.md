# Welcome to Atlas

[![Build Status](https://travis-ci.org/urbanetic/atlas.svg?branch=develop)](https://travis-ci.org/urbanetic/atlas)
[![Documentation Status](https://readthedocs.org/projects/atlasjs/badge/?version=latest)](https://readthedocs.org/projects/atlasjs/?badge=latest)

Atlas is a JavaScript library that provides a high-level API for 3D geospatial rendering. The Atlas
API makes it easy to incorporate modern rendering technologies in Web applications, supporting the
next generation of GIS tools.

[Documentation][docs] and [JSDocs][jsdocs].

[http://www.atlasjs.org/](http://www.atlasjs.org/)

## Getting Started

The default implementation of Atlas, `Atlas-Cesium`, uses the HTML5 Cesium rendering library. As a
result, getting started with Atlas is as simple as including some JavaScript into your Web app. See
[Documentation][docs] for more information.

## Demos

> TODO

## Contact

Atlas is developed by Urbanetic with support from the [Australian Urban Research Infrastructure
Network][aurin] (AURIN). If you're interested in using Atlas in your own project, or are having any
trouble, let us know at [hello@urbanetic.net][mail].

[docs]: http://docs.atlas.urbanetic.net
[jsdocs]: http://jsdocs.atlas.urbanetic.net
[aurin]: http://aurin.org.au
[mail]: mailto:hello@urbanetic.net
