define(
    [
      'atlas/model/GeoPoint', 'atlas/model/GeoEntity', 'atlas/model/Vertex',
      'atlas/util/WKT', 'atlas/util/DeveloperError', 'atlas/lib/utility/Setter',
      'atlas/events/Event', 'underscore'
    ],
    function(
        GeoPoint, GeoEntity, Vertex, WKT, DeveloperError, Setter, Event, _) {
      /**
       * @typedef atlas.model.Procedural
       * @ignore
       */
      var Procedural;

      /**
       * @classdesc A Procedural provides a way to visualise a Feature as a
       * procedurally generated geometry.
       */
      Procedural = GeoEntity.extend(/** @lends atlas.model.Procedural */ {

        _setup: function(id, data, args) {
          this._super(id, data, args);
          this._parameters = data;
          this._entities = [];
          this._footprint = null;
          this._masonSentence = null;
          this._entities = [];

          this._setVertices(data.vertices);
          this._masonSentence =
              Setter.def(data.masonSentence, Procedural.DEFAULT_MASON_SENTENCE);
        },

        _update: function() {
          this._super();
        },

        _build: function() {

          // Only build if the parent's display mode is procedural
          var parentFeature = this.getParent();
          if (parentFeature &&
              parentFeature.getDisplayMode() != "procedural") {
            return;
          }

          var isModelDirty = this.isDirty('entity') ||
              this.isDirty('vertices') || this.isDirty('model')
          var isStyleDirty = this.isDirty('style')
          if (isModelDirty) {
            _.each(this._entities, function(e) {
              this._entityManager.remove(e.getId());
            }, this);
            this._entities = [];

            var model = Brick.produce(
                this._masonSentence,
                _.extend(this._parameters, {vertices: this._vertices}));
            model = C3mlGen.generateFromInstance(model);

            _.each(model, function(m) {
              if (m.type !== 'POLYGON') return;

              var creationArgs = this._convertMasonGeometry(m);
              var constructArgs = {
                renderManager: this._renderManager,
                eventManager: this._eventManager,
                parent: this._id
              };
              this._entities.push(this._entityManager.createEntity(
                  creationArgs.id, creationArgs, constructArgs));
            }, this);
          }

          if (!this.isVisible()) {
            _.each(this._entities, function(e) {
              e.hide();
            }, this);
          }
        },

        // -------------------------------------------
        // GETTERS AND SETTERS
        // -------------------------------------------
        show: function() {
          wasVisible = this.isVisible();
          this._super();
          if (wasVisible) {
            return;
          }
          _.each(this._entities, function(e) {
            e.show();
          });
        },

        hide: function() {
          wasVisible = this.isVisible();
          this._super();
          if (!wasVisible) {
            return;
          }
          _.each(this._entities, function(e) {
            e.hide();
          });
        },

        getVertices: function() {
          return this._vertices;
        },

        _setVertices: function(vertices) {
          var verts;
          if (typeof (vertices) == 'string') {
            verts = WKT.getInstance().verticesFromWKT(vertices);
          } else if (Array.isArray(vertices)) {
            verts = vertices;
          } else {
            throw new DeveloperError(
                'Procedural can only take WKT or an array as vertices');
          }

          if (this._vertices !== verts) {
            this._vertices = verts;
            this._parameters['vertices'] = verts;
            return true;
          }
          return false;
        },

        setVertices: function(vertices) {
          if (this._setVertices(vertices)) {
            this.setDirty('vertices');
            this._update();
          }
        },

        setHeight: function(height) {
          this.setParameter('height', height);
          this.setDirty('height');
        },

        getMasonSentence: function() {
          return this._masonSentence;
        },

        setMasonSentence: function(sentence) {
          if (this._masonSentence !== sentence) {
            this._masonSentence = sentence;
            this.setDirty('model');
            this._update();
          }
        },

        getParameters: function() {
          return this._parameters;
        },

        getParameter: function(parameter) {
          return this._parameters[parameter];
        },

        setParameter: function(parameter, value) {
          if (this._parameters[parameter] !== value) {
            this._parameters[parameter] = value;
            this.setDirty('model');
            this._update();
          }
        },

        getEntities: function() {
          return this._entities;
        },

        _convertMasonGeometry: function(geo) {
          var classType = geo.geometry.scope.constructor.name;
          var converter = Procedural.MASON_CONVERTERS[classType];
          return converter.call(this, geo);
        },

        getOpenLayersGeometry: function(args) {
          var vertices = (args && args.vertices) || [...this.getVertices()];
          for (var i in vertices) {
            var v = vertices[i];
            vertices[i] = new GeoPoint([v[0], v[1], 0]);
          }
          var wkt = WKT.getInstance();
          if (args && args.utm) {
            vertices = vertices.map(function(point) {
              return point.toUtm().coord;
            });
            return wkt.openLayersPolygonFromVertices(vertices);
          } else {
            return wkt.openLayersPolygonFromGeoPoints(vertices);
          }
        },

        setStyle: function(style) {
          var previousStyle;
          _.each(this._entities, function(e) {
            previousStyle = e.setStyle(style);
          });
          return previousStyle;
        },

        getStyle: function() {
          return this._entities[0].getStyle();
        }
      });

      Procedural.DEFAULT_MASON_SENTENCE = 'parameter vertices\n' +
          'parameter height = 1\n' +
          'SimpleModule {\n' +
          '  scale 1, height, 1\n' +
          '  set-scope polygon y vertices\n' +
          '  box\n' +
          '}';

      Procedural.MASON_CONVERTERS = {
        Scope: function(entity) {},
        PolygonScope: function(entity) {
          var scope = entity.geometry.scope;
          var c3mlPolygon = scope.polygon.points;
          var vertices = [];
          _.each(c3mlPolygon, function(p) {
            vertices.push(new Vertex(p.x, p.y, scope.position));
          });
          var entityCreationArgs = {
            id: entity.id,
            vertices: vertices,
            type: 'polygon',
            height: entity.height
          };
          return entityCreationArgs;
        }
      };

      return Procedural;
    })
