define([
  'atlas/events/EventManager',
  'atlas/model/Procedural',
  'atlas/model/Vertex',
  'atlas/util/WKT'
], function(EventManager, Procedural, Vertex, WKT) {

  /*
   * Brick and C3mlGen (from grammar-prototype) are set in
   * window in production. Mock that behaviour here.
   */
  window.Brick = {
    produce: function(sentence, parameters) { }
  };
  window.C3mlGen = {
    generateFromInstance: function(model) { }
  };
  window.PolygonScope = (function() {
    function PolygonScope(args) {
      this.axis = args.axis;
      this.polygon = args.polygon;
      this.extrusion = args.extrusion;
      this.position = args.position;
    }
    return PolygonScope;
  })();
  window.Scope = (function() {
    function Scope(args) {
      this.position = args.P;
      this.size = args.S;
      this.rotation = args.R;
    }
    return Scope;
  })();
  window.Entity = (function() {
    function Entity(args) {
      this.name = args.name;
      this.type = args.type;
      this.id = args.id;
      this.properties = args.properties;
      this.parentId = args.parentId;
      this.geometry = args.geometry;
    };
    return Entity;
  })();
  MockEntityManager = (function() {
    function MockEntityManager() {
    };
    MockEntityManager.prototype.createEntity = function(e) {}
    MockEntityManager.prototype.add = function(id, creationArgs, constructionArgs) {}
    return MockEntityManager;
  })();

  describe('A Procedural', function() {
    var procedural, footprint, constructArgs, eventManager, vertices;

    createProcedural = function(args) {
      var id = 12345;
      eventManager = new EventManager({dom: {}, event: {}, render: {}});
      constructArgs = {
        renderManager: {},
        eventManager: eventManager,
        entityManager: new MockEntityManager()
      };
      return new Procedural(id, args, constructArgs);
    }

    beforeEach(function() {
      footprint = "POLYGON((0.0 0.0, 1.0 0.0, 1.0 1.0, 0.0, 1.0))";
      vertices = WKT.getInstance().verticesFromWKT(footprint);
    });

    afterEach(function () {
      procedural = eventManager = null;
    });

    describe('can be constructed', function() {
      it('with defaults', function() {
        var data = {
          vertices: footprint
        };
        procedural = createProcedural(data);

        expect(procedural.getVertices()).toEqual(vertices);
        expect(procedural.getMasonSentence()).toEqual(Procedural.DEFAULT_MASON_SENTENCE);
        expect(procedural.getParameters()).toEqual(data);
      });
    });

    describe('can use its getters and setters', function() {
      var Procedural__update = null;

      beforeEach(function() {
        var data = {
          vertices: footprint
        };
        procedural = createProcedural(data);
        Procedural__update = spyOn(Procedural.prototype, '_update');
      });

      afterEach(function() {
        Procedural__update = null;
      });

      it('to set its vertices', function() {
        var newFootprint = "POLYGON((1.0 1.0, 2.0 1.0, 2.0 2.0, 1.0, 2.0))";
        procedural.setVertices(newFootprint);
        expect(procedural.getVertices()).toEqual(
          WKT.getInstance().verticesFromWKT(newFootprint)
        );
        expect(procedural.isDirty('vertices')).toBeTrue();
        expect(Procedural__update).toHaveBeenCalled();
      });

      it('to set its mason sentence', function() {
        procedural.setMasonSentence("newSentence");
        expect(procedural.getMasonSentence()).toEqual("newSentence");
        expect(procedural.isDirty('model')).toBeTrue();
        expect(Procedural__update).toHaveBeenCalled();
      });

      it('to set its parameters', function() {
        procedural.setParameter("newPar", 123);
        expect(procedural.getParameter("newPar")).toEqual(123);
        expect(procedural.isDirty('model')).toBeTrue();
        expect(Procedural__update).toHaveBeenCalled();
      });

      it('to set its height', function() {
        procedural.setHeight(123);
        expect(procedural.getParameter("height")).toEqual(123);
        expect(procedural.isDirty('model')).toBeTrue();
        expect(Procedural__update).toHaveBeenCalled();
      });
    });

    describe('can convert from Mason models', function() {
      var polygon = [
        {x: 0, y: 0},
        {x: 1, y: 0},
        {x: 1, y: 1},
        {x: 0, y: 1},
      ];
      var C3mlGen_generateFromInstance;
      beforeEach(function() {
        C3mlGen_generateFromInstance = spyOn(window.C3mlGen, 'generateFromInstance');
      });
      afterEach(function() {
        C3mlGen_generateFromInstance = null;
      });

      it('from a Scope', function() {
        var model = {
          name: "Module",
          type: "POLYGON",
          geometry: {
            type: "primitive",
            primitiveType: "box",
            scope: new window.Scope({
              P: {x: 0, y: 0, z: 0},
              S: {x: 1, y: 2, z: 1},
              R: {x: 0, y: 0, z: 0}
            })
          }
        };
        var creationArgs = Procedural.MASON_CONVERTERS.Scope(model);
      });

      it('from a PolygonScope', function() {
        var polygonScopeEntity = new Entity({
          name: "Module",
          type: "POLYGON",
          parentId: "parent",
          id: "polygonScopeId",
          geometry: {
            type: "primitive",
            primitiveType: "box",
            scope: new window.PolygonScope({
              axis: "y",
              extrusion: 3,
              position: 0,
              polygon: { points:polygon }
            })
          }
        });
        var creationArgs = Procedural.MASON_CONVERTERS.PolygonScope(polygonScopeEntity);
        expect(creationArgs.id).toEqual("polygonScopeId");
        expect(creationArgs.vertices).toEqual([
          new Vertex(0, 0, 0),
          new Vertex(1, 0, 0),
          new Vertex(1, 1, 0),
          new Vertex(0, 1, 0),
        ]);
      });
    });

    describe('can create children entities', function() {
      var polygon = [
        {x: 0, y: 0},
        {x: 1, y: 0},
        {x: 1, y: 1},
        {x: 0, y: 1},
      ];
      var collectionEntity = new Entity({
        name: 'SimpleModule',
        type: 'COLLECTION',
        id: 'parent',
        properties: {},
        parentId: null,
        geometry: null
      });

      var C3mlGen_generateFromInstance;
      var EntityManager_createEntity;
      beforeEach(function() {
        C3mlGen_generateFromInstance = spyOn(window.C3mlGen, 'generateFromInstance');
        EntityManager_createEntity = spyOn(MockEntityManager.prototype, 'createEntity');
      });
      afterEach(function() {
        EntityManager_createEntity = C3mlGen_generateFromInstance = null;
      });

      it('from multiple entities', function() {
        var polygonScopeEntity = new Entity({
          name: "Module",
          type: "POLYGON",
          parentId: "parent",
          geometry: {
            type: "primitive",
            primitiveType: "box",
            scope: new window.PolygonScope({
              axis: "y",
              extrusion: 3,
              position: 0,
              polygon: { points:polygon }
            })
          }
        });
        C3mlGen_generateFromInstance.and.returnValue([collectionEntity, polygonScopeEntity, polygonScopeEntity]);
        var data = {
          vertices: footprint
        };
        var procedural = createProcedural(data);
        expect(EntityManager_createEntity).toHaveBeenCalled();
        expect(procedural.getEntities().length).toEqual(2);
      });
    });
  });
});
